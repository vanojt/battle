﻿namespace WindowsFormsApplication1
{
    partial class Multiplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Multiplayer));
            this.Back = new System.Windows.Forms.Button();
            this.Create = new System.Windows.Forms.Button();
            this.Connect = new System.Windows.Forms.Button();
            this.Volume = new System.Windows.Forms.TrackBar();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(320, 300);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(160, 40);
            this.Back.TabIndex = 0;
            this.Back.Text = "Назад";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.button1_Click);
            // 
            // Create
            // 
            this.Create.Location = new System.Drawing.Point(320, 254);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(160, 40);
            this.Create.TabIndex = 1;
            this.Create.Text = "Создать игру";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(320, 208);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(160, 40);
            this.Connect.TabIndex = 2;
            this.Connect.Text = "Присоединиться";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // Volume
            // 
            this.Volume.BackColor = System.Drawing.Color.SlateBlue;
            this.Volume.Location = new System.Drawing.Point(670, 535);
            this.Volume.Name = "Volume";
            this.Volume.Size = new System.Drawing.Size(111, 45);
            this.Volume.TabIndex = 6;
            this.Volume.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(670, 494);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(111, 35);
            this.button5.TabIndex = 16;
            this.button5.Text = "Справка";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Multiplayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 566);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Volume);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.Back);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Multiplayer";
            this.Text = "Многопользовательская игра";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Multiplayer_FormClosed);
            this.Load += new System.EventHandler(this.Multiplayer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button Create;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.TrackBar Volume;
        private System.Windows.Forms.Button button5;
    }
}