﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class connect : Form
    {
        Form parent;
        Form1 root;
        Single single_form;
        Bitmap error = (Bitmap)Bitmap.FromFile("../../err.png");
        Bitmap wait = (Bitmap)Bitmap.FromFile("../../wait.png");
        Socket s;
        TcpClient client;
        Thread thr;
        bool flag = false;
        string ip_addr;
        public connect(Form parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        public connect(Form parent, Form1 root)
        {
            this.root = root;
            this.parent = parent;
            InitializeComponent();
        }

        public connect()
        {
            InitializeComponent();
        }

        private void connect_Load(object sender, EventArgs e)
        {
            
        }

        private void Back_Click(object sender, EventArgs e)
        {
            parent.Show();
            this.Hide();
        }

        private void connect_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] ip = textBox1.Text.Split('.');
            bool flag = false;
            int buf_1;
            if (ip.Length==4)
            {
                flag = true;
                for (int i = 0; i < 4; i++)
                {
                    if(int.TryParse(ip[i],out buf_1))
                    {
                        buf_1 = int.Parse(ip[i]);
                        if (!((buf_1 <=255)&&(buf_1>=0)))
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            if(!flag)
            {
                pictureBox1.Image = error;
                return;
            }
            else
            {
                ip_addr = textBox1.Text;
                pictureBox1.Image = wait;
                //thr = new Thread(myfunc);
                //thr.Start();
                bool connection = false;
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        client = new TcpClient(ip_addr, 10500);
                        connection = true;
                        break;
                    }
                    catch
                    {}
                }
                if (!connection)
                {
                    pictureBox1.Image = error;
                    return;
                }
                else
                {
                    //MessageBox.Show("Подключен");
                    flag = true;
                    NetworkStream n = client.GetStream();
                    byte[] buf = new byte[1];
                    buf[0] = 1;
                    n.Write(buf, 0, 1);
                    //textBox1.Text = "";
                    this.Hide();
                    root.single_form = new Single(root, root, 2, client);
                    root.single_form.Show();
                }           


            }           
        }

        void myfunc()
        {
            
        }

        private void connect_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Close();
            root.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("help.html");
        }
    }
}
