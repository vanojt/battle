﻿namespace WindowsFormsApplication1
{
    partial class Single
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Single));
            this.Back = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.difficulty = new System.Windows.Forms.Label();
            this.hard = new System.Windows.Forms.RadioButton();
            this.medium = new System.Windows.Forms.RadioButton();
            this.easy = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ship1_4 = new System.Windows.Forms.PictureBox();
            this.ship1_3 = new System.Windows.Forms.PictureBox();
            this.ship1_2 = new System.Windows.Forms.PictureBox();
            this.ship1_1 = new System.Windows.Forms.PictureBox();
            this.ship2_2 = new System.Windows.Forms.PictureBox();
            this.ship2_3 = new System.Windows.Forms.PictureBox();
            this.ship2_1 = new System.Windows.Forms.PictureBox();
            this.ship3_2 = new System.Windows.Forms.PictureBox();
            this.ship3_1 = new System.Windows.Forms.PictureBox();
            this.ship4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.autorasst = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button5 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.read = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(12, 522);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(120, 35);
            this.Back.TabIndex = 0;
            this.Back.Text = "Назад";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 522);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(351, 351);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.difficulty);
            this.panel1.Controls.Add(this.hard);
            this.panel1.Controls.Add(this.medium);
            this.panel1.Controls.Add(this.easy);
            this.panel1.Location = new System.Drawing.Point(690, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(82, 90);
            this.panel1.TabIndex = 4;
            // 
            // difficulty
            // 
            this.difficulty.AutoSize = true;
            this.difficulty.Location = new System.Drawing.Point(3, 8);
            this.difficulty.Name = "difficulty";
            this.difficulty.Size = new System.Drawing.Size(63, 13);
            this.difficulty.TabIndex = 3;
            this.difficulty.Text = "Сложность";
            // 
            // hard
            // 
            this.hard.AutoSize = true;
            this.hard.Location = new System.Drawing.Point(3, 70);
            this.hard.Name = "hard";
            this.hard.Size = new System.Drawing.Size(63, 17);
            this.hard.TabIndex = 2;
            this.hard.Text = "сложно";
            this.hard.UseVisualStyleBackColor = true;
            // 
            // medium
            // 
            this.medium.AutoSize = true;
            this.medium.Checked = true;
            this.medium.Location = new System.Drawing.Point(3, 47);
            this.medium.Name = "medium";
            this.medium.Size = new System.Drawing.Size(81, 17);
            this.medium.TabIndex = 1;
            this.medium.TabStop = true;
            this.medium.Text = "нормально";
            this.medium.UseVisualStyleBackColor = true;
            // 
            // easy
            // 
            this.easy.AutoSize = true;
            this.easy.Location = new System.Drawing.Point(3, 24);
            this.easy.Name = "easy";
            this.easy.Size = new System.Drawing.Size(54, 17);
            this.easy.TabIndex = 0;
            this.easy.Text = "легко";
            this.easy.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.ship1_4);
            this.panel2.Controls.Add(this.ship1_3);
            this.panel2.Controls.Add(this.ship1_2);
            this.panel2.Controls.Add(this.ship1_1);
            this.panel2.Controls.Add(this.ship2_2);
            this.panel2.Controls.Add(this.ship2_3);
            this.panel2.Controls.Add(this.ship2_1);
            this.panel2.Controls.Add(this.ship3_2);
            this.panel2.Controls.Add(this.ship3_1);
            this.panel2.Controls.Add(this.ship4);
            this.panel2.Location = new System.Drawing.Point(369, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(247, 186);
            this.panel2.TabIndex = 5;
            // 
            // ship1_4
            // 
            this.ship1_4.BackColor = System.Drawing.Color.Transparent;
            this.ship1_4.Image = ((System.Drawing.Image)(resources.GetObject("ship1_4.Image")));
            this.ship1_4.Location = new System.Drawing.Point(126, 147);
            this.ship1_4.Name = "ship1_4";
            this.ship1_4.Size = new System.Drawing.Size(34, 34);
            this.ship1_4.TabIndex = 9;
            this.ship1_4.TabStop = false;
            this.ship1_4.Tag = "9 1";
            this.ship1_4.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship1_3
            // 
            this.ship1_3.BackColor = System.Drawing.Color.Transparent;
            this.ship1_3.Image = ((System.Drawing.Image)(resources.GetObject("ship1_3.Image")));
            this.ship1_3.Location = new System.Drawing.Point(85, 147);
            this.ship1_3.Name = "ship1_3";
            this.ship1_3.Size = new System.Drawing.Size(34, 34);
            this.ship1_3.TabIndex = 8;
            this.ship1_3.TabStop = false;
            this.ship1_3.Tag = "8 1";
            this.ship1_3.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship1_2
            // 
            this.ship1_2.BackColor = System.Drawing.Color.Transparent;
            this.ship1_2.Image = ((System.Drawing.Image)(resources.GetObject("ship1_2.Image")));
            this.ship1_2.Location = new System.Drawing.Point(44, 147);
            this.ship1_2.Name = "ship1_2";
            this.ship1_2.Size = new System.Drawing.Size(34, 34);
            this.ship1_2.TabIndex = 7;
            this.ship1_2.TabStop = false;
            this.ship1_2.Tag = "7 1";
            this.ship1_2.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship1_1
            // 
            this.ship1_1.BackColor = System.Drawing.Color.Transparent;
            this.ship1_1.Image = ((System.Drawing.Image)(resources.GetObject("ship1_1.Image")));
            this.ship1_1.Location = new System.Drawing.Point(3, 147);
            this.ship1_1.Name = "ship1_1";
            this.ship1_1.Size = new System.Drawing.Size(34, 34);
            this.ship1_1.TabIndex = 6;
            this.ship1_1.TabStop = false;
            this.ship1_1.Tag = "6 1";
            this.ship1_1.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship2_2
            // 
            this.ship2_2.BackColor = System.Drawing.Color.Transparent;
            this.ship2_2.Image = ((System.Drawing.Image)(resources.GetObject("ship2_2.Image")));
            this.ship2_2.Location = new System.Drawing.Point(167, 3);
            this.ship2_2.Name = "ship2_2";
            this.ship2_2.Size = new System.Drawing.Size(34, 72);
            this.ship2_2.TabIndex = 5;
            this.ship2_2.TabStop = false;
            this.ship2_2.Tag = "4 2";
            this.ship2_2.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship2_3
            // 
            this.ship2_3.BackColor = System.Drawing.Color.Transparent;
            this.ship2_3.Image = ((System.Drawing.Image)(resources.GetObject("ship2_3.Image")));
            this.ship2_3.Location = new System.Drawing.Point(208, 3);
            this.ship2_3.Name = "ship2_3";
            this.ship2_3.Size = new System.Drawing.Size(34, 72);
            this.ship2_3.TabIndex = 4;
            this.ship2_3.TabStop = false;
            this.ship2_3.Tag = "5 2";
            this.ship2_3.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship2_1
            // 
            this.ship2_1.BackColor = System.Drawing.Color.Transparent;
            this.ship2_1.Image = ((System.Drawing.Image)(resources.GetObject("ship2_1.Image")));
            this.ship2_1.Location = new System.Drawing.Point(126, 3);
            this.ship2_1.Name = "ship2_1";
            this.ship2_1.Size = new System.Drawing.Size(34, 72);
            this.ship2_1.TabIndex = 3;
            this.ship2_1.TabStop = false;
            this.ship2_1.Tag = "3 2";
            this.ship2_1.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship3_2
            // 
            this.ship3_2.BackColor = System.Drawing.Color.Transparent;
            this.ship3_2.Image = ((System.Drawing.Image)(resources.GetObject("ship3_2.Image")));
            this.ship3_2.Location = new System.Drawing.Point(85, 3);
            this.ship3_2.Name = "ship3_2";
            this.ship3_2.Size = new System.Drawing.Size(34, 106);
            this.ship3_2.TabIndex = 2;
            this.ship3_2.TabStop = false;
            this.ship3_2.Tag = "2 3";
            this.ship3_2.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship3_1
            // 
            this.ship3_1.BackColor = System.Drawing.Color.Transparent;
            this.ship3_1.Image = ((System.Drawing.Image)(resources.GetObject("ship3_1.Image")));
            this.ship3_1.Location = new System.Drawing.Point(44, 3);
            this.ship3_1.Name = "ship3_1";
            this.ship3_1.Size = new System.Drawing.Size(34, 106);
            this.ship3_1.TabIndex = 1;
            this.ship3_1.TabStop = false;
            this.ship3_1.Tag = "1 3";
            this.ship3_1.Click += new System.EventHandler(this.ship4_Click);
            // 
            // ship4
            // 
            this.ship4.BackColor = System.Drawing.Color.Transparent;
            this.ship4.Image = ((System.Drawing.Image)(resources.GetObject("ship4.Image")));
            this.ship4.Location = new System.Drawing.Point(3, 3);
            this.ship4.Name = "ship4";
            this.ship4.Size = new System.Drawing.Size(34, 139);
            this.ship4.TabIndex = 0;
            this.ship4.TabStop = false;
            this.ship4.Tag = "0 4";
            this.ship4.Click += new System.EventHandler(this.ship4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 546);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // autorasst
            // 
            this.autorasst.Location = new System.Drawing.Point(12, 369);
            this.autorasst.Name = "autorasst";
            this.autorasst.Size = new System.Drawing.Size(120, 35);
            this.autorasst.TabIndex = 7;
            this.autorasst.Text = "Авторасстановка";
            this.autorasst.UseVisualStyleBackColor = true;
            this.autorasst.Click += new System.EventHandler(this.autorasst_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(138, 369);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 35);
            this.button2.TabIndex = 8;
            this.button2.Text = "В бой";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(264, 369);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 35);
            this.button3.TabIndex = 9;
            this.button3.Text = "Сброс";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 481);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 35);
            this.button4.TabIndex = 10;
            this.button4.Text = "Загрузить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.SlateBlue;
            this.trackBar1.Location = new System.Drawing.Point(670, 535);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(111, 45);
            this.trackBar1.TabIndex = 11;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(670, 494);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(111, 35);
            this.button5.TabIndex = 15;
            this.button5.Text = "Справка";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(500, 321);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 16;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // read
            // 
            this.read.Enabled = false;
            this.read.Location = new System.Drawing.Point(138, 410);
            this.read.Name = "read";
            this.read.Size = new System.Drawing.Size(120, 35);
            this.read.TabIndex = 17;
            this.read.Text = "Готов";
            this.read.UseVisualStyleBackColor = true;
            this.read.Visible = false;
            this.read.Click += new System.EventHandler(this.read_Click);
            // 
            // Single
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 566);
            this.Controls.Add(this.read);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.autorasst);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Back);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Single";
            this.Text = "Расстановка кораблей";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Single_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Single_FormClosed);
            this.Load += new System.EventHandler(this.Single_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ship1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ship4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label difficulty;
        private System.Windows.Forms.RadioButton hard;
        private System.Windows.Forms.RadioButton medium;
        private System.Windows.Forms.RadioButton easy;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox ship1_4;
        private System.Windows.Forms.PictureBox ship1_3;
        private System.Windows.Forms.PictureBox ship1_2;
        private System.Windows.Forms.PictureBox ship1_1;
        private System.Windows.Forms.PictureBox ship2_2;
        private System.Windows.Forms.PictureBox ship2_3;
        private System.Windows.Forms.PictureBox ship2_1;
        private System.Windows.Forms.PictureBox ship3_2;
        private System.Windows.Forms.PictureBox ship3_1;
        private System.Windows.Forms.PictureBox ship4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button autorasst;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button read;
    }
}