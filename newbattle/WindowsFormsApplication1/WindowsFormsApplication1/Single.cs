﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Single : Form
    {
        bool me_ready = false;
        Socket s;
        Form parent_c;
        Form1 parent;
        TcpListener listener;
        NetworkStream n;
        TcpClient client;
        Thread thr,thr1;
        Bitmap pole;
        int ready = 0;
        public result res;
        int x_cell_prev = 1;
        int y_cell_prev = 1;
        PictureBox[] ships_im = new PictureBox[10];
        int[,] field = new int[10, 10];
        int active_ship_id = -1;
        int active_ship_size = 0;
        bool povorot = false;
        Ship[] ships_place = new Ship[10];
        Ship[] ships_place_enemy = new Ship[10];
        bool[] ships_flag = new bool[10];/*
                           0 - 4п
                           1 - 3п_1
                           2 - 3п_2
                           3 - 2п_1
                           4 - 2п_2
                           5 - 2п_3
                           6 - 1п_1
                           7 - 1п_2
                           8 - 1п_3
                           9 - 1п_4
                           */
        string[] ids;
        PictureBox active_ship;
        int mult_flag = 0;
        public Single()
        {
            InitializeComponent();
        }

        public Single (Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        public Single (Form1 root, Form parent, int flag)//многопользовательский
        {
            this.parent = root;
            InitializeComponent();
            this.parent_c = parent;
            panel1.Visible = false;
            mult_flag = flag;
            button4.Visible = false;
            read.Visible = true;
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            thr = new Thread(Connection_server);
            thr.Start();

        }

        public Single(Form1 root, Form parent, int flag,TcpClient client)//многопользовательский
        {
            this.client = client;
            this.parent = root;
            InitializeComponent();
            this.parent_c = parent;
            panel1.Visible = false;
            mult_flag = flag;
            button4.Visible = false;
            read.Visible = true;
            n = client.GetStream();
            thr = new Thread(Connection_client);
            thr.Start();

        }

        public void Connection_client()
        {
            byte[] byte_buff = new byte[1];
            try
            {
                while (true)
                {
                    n.Read(byte_buff, 0, 1);
                    if (checkBox1.InvokeRequired)
                    {
                        checkBox1.BeginInvoke(new check1(check));
                    }
                }
            }
            catch (Exception e)
            { //MessageBox.Show(e.Message);}
            }
        }

        public void Connection_server()
        {
            listener = new TcpListener(10500);
            try
            {
                listener.Start();
                s = listener.AcceptSocket();
                byte[] buffer_sock = new byte[1];
                s.Receive(buffer_sock);//флаг установления подключения
                //MessageBox.Show(buffer_sock[0].ToString());
                while(true)
                {
                    s.Receive(buffer_sock);
                    if (checkBox1.InvokeRequired)
                    {
                        checkBox1.BeginInvoke(new check1(check));
                    }
                }
            }
            catch(Exception e)
            {
                //MessageBox.Show(e.Message);
            }
        }

        public delegate void check1();
        public void check()
        {
            checkBox1.Checked = !checkBox1.Checked;
        }

        public void wait_enemy_client()
        {
            byte[] byte_buff = new byte[1];
            int x = n.Read(byte_buff,0,1);
            MessageBox.Show(byte_buff[0].ToString());
        }

        public void wait_enemy_server()
        {
            byte[] byte_buff = new byte[1];
            int x = n.Read(byte_buff, 0, 1);
            MessageBox.Show(byte_buff[0].ToString());
        }

        public static int GetCoordX(MouseEventArgs e)//получить номер ячейки по X
        {
            int x = e.X;
            x++;
            x = (x / 35) + 1;
            if (x == 11)
            {
                x--;
            }
            return x;
        }

        public static int GetCoordY(MouseEventArgs e)//получить номер ячейки по Y
        {
            int y = e.Y;
            y++;
            y = (y / 35) + 1;
            if (y == 11)
            {
                y--;
            }
            return y;
        }

        public static Bitmap ColorFillCell (int x, int y, Color fillColor, Bitmap map)//(индекс с 1)залить ячейку цветом
        {
            x--;
            y--;
            int i_begin = x * 35 + 1;
            int i_end = (x + 1) * 35;

            int j_begin = y * 35 + 1;
            int j_end = (y + 1) * 35;
            for (int i = i_begin; i < i_end; i++)
            {
                for (int j = j_begin; j < j_end; j++)
                {
                    
                    map.SetPixel(i, j, fillColor);
                }
            }
            return map;
        }

        private bool ReadyCheck()//проверка готовности к бою
        {
            bool res = true;
            for(int i = 0; i < 10; i++)
            {
                if (ships_im[i].Enabled)
                {
                    res = false;
                }
            }
            return res;
        }

        private bool CanPaint(int x_cell, int y_cell, int ship_size, bool povorot)
        {
            bool res = true;
            for (int i = 0; i < ship_size; i++)
            {
                if (!povorot)//проверка по вертикали
                {
                    if (field[x_cell-1,y_cell-1 + i]!=-1)
                    {
                        return false;
                    }
                }
                else
                {
                    if (field[x_cell - 1 + i, y_cell - 1] != -1)
                    {
                        return false;
                    }
                }
            }
            return res;
        }

        private bool CanPaint(int x_cell, int y_cell, int ship_size, bool povorot, int[,] b_field)
        {
            bool res = true;
            for (int i = 0; i < ship_size; i++)
            {
                if (!povorot)//проверка по вертикали
                {
                    if (b_field[x_cell - 1, y_cell - 1 + i] != -1)
                    {
                        return false;
                    }
                }
                else
                {
                    if (b_field[x_cell - 1 + i, y_cell - 1] != -1)
                    {
                        return false;
                    }
                }
            }
            return res;
        }

        private List<int>[] GetDrawableCells(int size,bool pov, int[,] b_field)
        {
            List<int> res_x = new List<int>();
            List<int> res_y = new List<int>();
            List<int>[] res = new List<int>[2];
            if (!pov)
            {
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 11 - size; j++)
                    {
                        if (CanPaint(i + 1, j + 1, size, pov, b_field))
                        {
                            res_x.Add(i);
                            res_y.Add(j);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < 11 - size; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (CanPaint(i + 1, j + 1, size, pov, b_field))
                        {
                            res_x.Add(i);
                            res_y.Add(j);
                        }
                    }
                }
            }
            
            res[0] = res_x;
            res[1] = res_y;
            return res;
        }


        private Bitmap PlaceShip(int x, int y, int ship_id, Bitmap map)//считать индекс с 1
        {
            Bitmap image = (Bitmap)ships_im[ship_id].Image;
            ids = ships_im[ship_id].Tag.ToString().Split();
            int size = int.Parse(ids[1]);
            if(!povorot)//если вертикально
            {
                x--;//чтоб соответствовать индексу
                y--;//чтоб соответствовать индексу
                int i_begin = x * 35 + 1;
                int i_end = i_begin + image.Width;

                int j_begin = y * 35 + 1;
                int j_end = j_begin + image.Height;
                for (int i = i_begin; i < i_end; i++)
                {
                    for (int j = j_begin; j < j_end; j++)
                    {
                        if (j%35==0)//проверка на сетку
                        {
                            map.SetPixel(i, j, Color.Black);
                        }
                        else
                        {
                            map.SetPixel(i, j, image.GetPixel(i - i_begin, j - j_begin));
                        }
                    }
                }
                for (int i = 0; i < size; i++)
                {
                    field[x, y + i] = ship_id;//заполнение ячеек айдишниками корабля
                }
                if (x+1<10)
                {
                    for (int i = 0; i < size; i++)
                    {
                        field[x+1, y + i] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (x - 1 >= 0)
                {
                    for (int i = 0; i < size; i++)
                    {
                        field[x - 1, y + i] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (y + size < 10)
                {
                    field[x, y + size] += -1;
                    if (x + 1 < 10)
                    {
                        field[x + 1, y + size] += -1;
                    }
                    if (x - 1 >= 0)
                    {
                        field[x - 1, y + size] += -1;
                    }
                }
                if (y - 1 >= 0)
                {
                    field[x, y - 1] += -1;
                    if (x + 1 < 10)
                    {
                        field[x + 1, y - 1] += -1;
                    }
                    if (x - 1 >= 0)
                    {
                        field[x - 1, y - 1] += -1;
                    }
                }
            }
            else//если горизонтально
            {
                x--;//чтоб соответствовать индексу
                y--;//чтоб соответствовать индексу
                int i_begin = x * 35 + 1;
                int i_end = i_begin + image.Height;

                int j_begin = y * 35 + 1;
                int j_end = j_begin + image.Width;
                for (int i = i_begin; i < i_end; i++)
                {
                    for (int j = j_begin; j < j_end; j++)
                    {
                        if (i%35==0)//проверка на сетку
                        {
                            map.SetPixel(i, j, Color.Black);
                        }
                        else
                        {
                            map.SetPixel(i, j, image.GetPixel(j - j_begin, i - i_begin));
                        }
                    }
                }
                for (int i = 0; i < size; i++)
                {
                    field[x+i, y] = ship_id;//заполнение айдишниками корабля
                }

                if (y + 1 < 10)
                {
                    for (int i = 0; i < size; i++)
                    {
                        field[x + i, y + 1] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (y - 1 >= 0)
                {
                    for (int i = 0; i < size; i++)
                    {
                        field[x + i, y - 1] += -1;//запрет для установки соседним клеткам
                    }
                }

                if (x + size < 10)
                {
                    field[x + size, y] += -1;
                    if (y + 1 < 10)
                    {
                        field[x + size, y + 1] += -1;
                    }
                    if (y - 1 >= 0)
                    {
                        field[x + size, y - 1] += -1;
                    }
                }
                if (x - 1 >= 0)
                {
                    field[x - 1, y] += -1;
                    if (y + 1 < 10)
                    {
                        field[x - 1, y + 1] += -1;
                    }
                    if (y - 1 >= 0)
                    {
                        field[x - 1, y - 1] += -1;
                    }
                }
            }
            return map;
        }

        private int[,] PlaceShip(int x, int y, int ship_id, int[,] b_field, int size, bool povorot)
        {
            x--;
            y--;
            if(!povorot)
            {
                for (int i = 0; i < size; i++)
                {
                    b_field[x, y + i] = ship_id;//заполнение ячеек айдишниками корабля
                }
                if (x + 1 < 10)
                {
                    for (int i = 0; i < size; i++)
                    {
                        b_field[x + 1, y + i] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (x - 1 >= 0)
                {
                    for (int i = 0; i < size; i++)
                    {
                        b_field[x - 1, y + i] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (y + size < 10)
                {
                    b_field[x, y + size] += -1;
                    if (x + 1 < 10)
                    {
                        b_field[x + 1, y + size] += -1;
                    }
                    if (x - 1 >= 0)
                    {
                        b_field[x - 1, y + size] += -1;
                    }
                }
                if (y - 1 >= 0)
                {
                    b_field[x, y - 1] += -1;
                    if (x + 1 < 10)
                    {
                        b_field[x + 1, y - 1] += -1;
                    }
                    if (x - 1 >= 0)
                    {
                        b_field[x - 1, y - 1] += -1;
                    }
                }
            }
            else
            {
                for (int i = 0; i < size; i++)
                {
                    b_field[x + i, y] = ship_id;//заполнение айдишниками корабля
                }

                if (y + 1 < 10)
                {
                    for (int i = 0; i < size; i++)
                    {
                        b_field[x + i, y + 1] += -1;//запрет для установки соседним клеткам
                    }
                }
                if (y - 1 >= 0)
                {
                    for (int i = 0; i < size; i++)
                    {
                        b_field[x + i, y - 1] += -1;//запрет для установки соседним клеткам
                    }
                }

                if (x + size < 10)
                {
                    b_field[x + size, y] += -1;
                    if (y + 1 < 10)
                    {
                        b_field[x + size, y + 1] += -1;
                    }
                    if (y - 1 >= 0)
                    {
                        b_field[x + size, y - 1] += -1;
                    }
                }
                if (x - 1 >= 0)
                {
                    b_field[x - 1, y] += -1;
                    if (y + 1 < 10)
                    {
                        b_field[x - 1, y + 1] += -1;
                    }
                    if (y - 1 >= 0)
                    {
                        b_field[x - 1, y - 1] += -1;
                    }
                }
            }
            return b_field;
        }

        private void RemoveShip(Ship curr_ship)//освобождение ячеек
        {
            int x = curr_ship.x_cell;
            int y = curr_ship.y_cell;
            int current_size = curr_ship.ship_size;
            bool f = curr_ship.povorot;
            x--;
            y--;
            for (int i = 0; i < current_size; i++)//освобождение ячеек
            {
                if(!f)
                {
                    field[x, y + i] = -1;
                }
                else
                {
                    field[x + i, y] = -1;
                }
                
            }
            if (!f)//вертикально
            {
                for (int i = 0; i < current_size; i++)
                {
                    pictureBox1.Image = ColorFillCell(x + 1, y + 1 + i, Color.White, (Bitmap)pictureBox1.Image);
                }

                if (x + 1 < 10)
                {
                    for (int i = 0; i < current_size; i++)
                    {
                        field[x + 1, y + i] += 1;//запрет для установки соседним клеткам
                    }
                }
                if (x - 1 >= 0)
                {
                    for (int i = 0; i < current_size; i++)
                    {
                        field[x - 1, y + i] += 1;//запрет для установки соседним клеткам
                    }
                }
                if (y + current_size < 10)
                {
                    field[x, y + current_size] += 1;
                    if (x + 1 < 10)
                    {
                        field[x + 1, y + current_size] += 1;
                    }
                    if (x - 1 >= 0)
                    {
                        field[x - 1, y + current_size] += 1;
                    }
                }
                if (y - 1 >= 0)
                {
                    field[x, y - 1] += 1;
                    if (x + 1 < 10)
                    {
                        field[x + 1, y - 1] += 1;
                    }
                    if (x - 1 >= 0)
                    {
                        field[x - 1, y - 1] += 1;
                    }
                }
            }
            else//горизонтально
            {
                for (int i = 0; i < current_size; i++)
                {
                    pictureBox1.Image = ColorFillCell(x + 1 + i, y + 1, Color.White, (Bitmap)pictureBox1.Image);
                }

                if (y + 1 < 10)
                {
                    for (int i = 0; i < current_size; i++)
                    {
                        field[x + i, y + 1] += 1;//запрет для установки соседним клеткам
                    }
                }
                if (y - 1 >= 0)
                {
                    for (int i = 0; i < current_size; i++)
                    {
                        field[x + i, y - 1] += 1;//запрет для установки соседним клеткам
                    }
                }

                if (x + current_size < 10)
                {
                    field[x + current_size, y] += 1;
                    if (y + 1 < 10)
                    {
                        field[x + current_size, y + 1] += 1;
                    }
                    if (y - 1 >= 0)
                    {
                        field[x + current_size, y - 1] += 1;
                    }
                }
                if (x - 1 >= 0)
                {
                    field[x - 1, y] += 1;
                    if (y + 1 < 10)
                    {
                        field[x - 1, y + 1] += 1;
                    }
                    if (y - 1 >= 0)
                    {
                        field[x - 1, y - 1] += 1;
                    }
                }
            }

        }

        public void Reset()
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    pictureBox1.Image = ColorFillCell(i, j, Color.White, (Bitmap)pictureBox1.Image);
                    field[i - 1, j - 1] = -1;
                }
                ships_im[i - 1].Enabled = true;
                ships_im[i - 1].Visible = true;
                ships_place[i - 1] = null;
                active_ship_id = -1;
                povorot = false;
                active_ship_size = 0;
                Deselect(ships_im[i - 1]);
            }
            button2.Enabled = false;
            read.Enabled = false;
        }

        private void Select(PictureBox box)
        {
            Bitmap map = (Bitmap)box.Image;
            for (int i = 0; i < map.Width-1; i++)
            {
                map.SetPixel(i, 0, Color.Red);
                map.SetPixel(i, map.Height - 1, Color.Red);
            }
            for (int i = 0; i < map.Height; i++)
            {
                map.SetPixel(0, i, Color.Red);
                map.SetPixel(map.Width - 1, i, Color.Red);
            }
            box.Image = map;
        }

        private void Deselect(PictureBox box)
        {
            Bitmap map = (Bitmap)box.Image;
            for (int i = 0; i < map.Width; i++)
            {
                map.SetPixel(i, 0, Color.FromArgb(0, 255, 255, 255));
                map.SetPixel(i, map.Height - 1, Color.FromArgb(0,255,255,255));
            }
            for (int i = 0; i < map.Height; i++)
            {
                map.SetPixel(0, i, Color.FromArgb(0, 255, 255, 255));
                map.SetPixel(map.Width - 1, i, Color.FromArgb(0, 255, 255, 255));
            }
            box.Image = map;
        }

        private void Single_Load(object sender, EventArgs e)
        {
            ships_im[0] = ship4;
            ships_im[1] = ship3_1;
            ships_im[2] = ship3_2;
            ships_im[3] = ship2_1;
            ships_im[4] = ship2_2;
            ships_im[5] = ship2_3;
            ships_im[6] = ship1_1;
            ships_im[7] = ship1_2;
            ships_im[8] = ship1_3;
            ships_im[9] = ship1_4;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    field[i, j] = -1;//-1 - свободная клетка -2 - запрещенная клетка 0-9 - занятая клетка
                }
            }
        }

        private void Single_FormClosed(object sender, FormClosedEventArgs e)
        {
            //e.Cancel = true;
            parent.CloseAll();            
        }

        private void Back_Click(object sender, EventArgs e)
        {
            try
            {
                s.Close();
                listener.Stop();
            }
            catch { }
            if (mult_flag>0)
            {
                Reset();
                parent_c.Show();
                this.Hide();
                mult_flag = 0;
            }
            else
            {
                Reset();
                parent.Show();
                this.Hide();
            }            
        }

        private void Single_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            //parent.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MouseEventArgs x = (MouseEventArgs)e;
            int x_cell = GetCoordX(x);
            int y_cell = GetCoordY(x);
            if (x.Button == MouseButtons.Right)//поворот корабля
            {
                if (active_ship_id!=-1)
                {
                    if(!povorot)
                    {
                        int x_cell_old = x_cell;
                        if (x_cell > 11 - active_ship_size)
                        {
                            x_cell = 11 - active_ship_size;
                        }
                        if (y_cell > 11 - active_ship_size)
                        {
                            y_cell = 11 - active_ship_size;
                        }
                        if (CanPaint(x_cell_old, y_cell, active_ship_size,povorot))
                        {
                            for (int i = 0; (i < active_ship_size) && (y_cell + i <= 10); i++)
                            {
                                pictureBox1.Image = ColorFillCell(x_cell_old, y_cell + i, Color.White, (Bitmap)pictureBox1.Image);
                            }
                        }
                    }
                    else
                    {
                        int y_cell_old = y_cell;
                        if (y_cell > 11 - active_ship_size)
                        {
                            y_cell = 11 - active_ship_size;
                        }
                        if (x_cell > 11 - active_ship_size)
                        {
                            x_cell = 11 - active_ship_size;
                        }
                        if (CanPaint(x_cell, y_cell_old, active_ship_size, povorot))
                        {
                            for (int i = 0; (i < active_ship_size) && (x_cell + i <= 10); i++)
                            {
                                pictureBox1.Image = ColorFillCell(x_cell + i, y_cell_old, Color.White, (Bitmap)pictureBox1.Image);
                            }
                        }
                    }
                }
                povorot = !povorot;
                x_cell_prev = x_cell;
                y_cell_prev = y_cell;
            }
            else
            {
                if (active_ship_id != -1)//установка корабля
                {
                    if (!povorot)
                    {
                        if (y_cell > 11 - active_ship_size)
                        {
                            y_cell = 11 - active_ship_size;
                        }
                    }
                    else
                    {
                        if (x_cell > 11 - active_ship_size)
                        {
                            x_cell = 11 - active_ship_size;
                        }
                    }
                    if(!CanPaint(x_cell,y_cell,active_ship_size,povorot))//если нельзя поставить, то выход
                    {
                        return;
                    }
                    ships_place[active_ship_id] = new Ship(x_cell, y_cell, active_ship_size, povorot);//занесение корабля в массив установленных
                    Deselect(ships_im[active_ship_id]);//снятие выделения
                    ships_im[active_ship_id].Enabled = false;//выключение поставленного корабля
                    ships_im[active_ship_id].Visible = false;//сокрытие поставленного корабля
                    pictureBox1.Image = this.PlaceShip(x_cell, y_cell, active_ship_id, (Bitmap)pictureBox1.Image);//установка по координатам
                    x_cell_prev = 1;
                    y_cell_prev = 1;
                    active_ship_id = -1;
                    active_ship_size = 1;
                    povorot = false;
                    if (ReadyCheck())
                    {
                        button2.Enabled = true;
                        read.Enabled = true;
                    }
                }
                else//перемещение корабля
                {
                    //MessageBox.Show(field[x_cell - 1, y_cell - 1].ToString());
                    if (field[x_cell-1,y_cell-1]>=0)
                    {
                        active_ship_id = field[x_cell-1, y_cell-1];
                        Select(ships_im[active_ship_id]);
                        ships_im[active_ship_id].Enabled = true;
                        ships_im[active_ship_id].Visible = true;
                        ids = ships_im[active_ship_id].Tag.ToString().Split();
                        ids = ships_im[active_ship_id].Tag.ToString().Split();
                        active_ship_size = int.Parse(ids[1]);
                        RemoveShip(ships_place[active_ship_id]);
                        button2.Enabled = false;
                        read.Enabled = false;
                        //if (!ships_place[active_ship_id].povorot)//если вертикально
                        //{
                        //    for (int i = 0; i < ships_place[active_ship_id].ship_size;i++)
                        //    {
                        //        field[ships_place[active_ship_id].x_cell - 1, ships_place[active_ship_id].y_cell - 1 + i] = -1;
                        //    }
                        //}
                        //else//если горизонтально
                        //{
                        //    for (int i = 0; i < ships_place[active_ship_id].ship_size; i++)
                        //    {
                        //        field[ships_place[active_ship_id].x_cell - 1 + i, ships_place[active_ship_id].y_cell - 1] = -1;
                        //    }
                        //}
                    }
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;
            int x_cell = GetCoordX(e);
            int y_cell = GetCoordY(e);
            label1.Text = e.X.ToString() + " " + e.Y.ToString() + " " + x_cell + " " + y_cell;
            pole = (Bitmap) pictureBox1.Image;
            if (active_ship_id!=-1)
            {
                if (!povorot)//вертикально
                {
                    if (y_cell>11-active_ship_size)
                    {
                        y_cell = 11 - active_ship_size;//если граничный случай, то не смещаем силуэт
                    }
                    if (CanPaint(x_cell, y_cell, active_ship_size, povorot))
                    {
                        for (int i = 0; i < active_ship_size; i++)
                            pictureBox1.Image = ColorFillCell(x_cell, y_cell + i, Color.Red, (Bitmap)pictureBox1.Image);
                    }
                    if ((x_cell != x_cell_prev) || (y_cell != y_cell_prev))//если мышка переведена на другую клетку
                    {
                        if (CanPaint(x_cell_prev, y_cell_prev, active_ship_size, povorot))
                        {
                            for (int i = 0; i < active_ship_size; i++)
                                pictureBox1.Image = ColorFillCell(x_cell_prev, y_cell_prev + i, Color.White, (Bitmap)pictureBox1.Image);
                        }
                        label2.Text = CanPaint(x_cell, y_cell, active_ship_size, povorot).ToString();
                        x_cell_prev = x_cell;
                        y_cell_prev = y_cell;                        
                    }
                }
                else if (povorot)//горизонтально
                {
                    if (x_cell > 11 - active_ship_size)
                    {
                        x_cell = 11 - active_ship_size;//если граничный случай, то не смещаем силуэт
                    }
                    if (CanPaint(x_cell, y_cell, active_ship_size, povorot))
                    {
                        for (int i = 0; i < active_ship_size; i++)
                            pictureBox1.Image = ColorFillCell(x_cell + i, y_cell, Color.Red, (Bitmap)pictureBox1.Image);
                    }
                    if ((x_cell != x_cell_prev) || (y_cell != y_cell_prev))//если мышь переведена на др клетку
                    {
                        if (x_cell_prev+y_cell_prev != -2)
                        {
                            if (CanPaint(x_cell_prev, y_cell_prev, active_ship_size, povorot))
                            {
                                for (int i = 0; i < active_ship_size; i++)
                                    pictureBox1.Image = ColorFillCell(x_cell_prev + i, y_cell_prev, Color.White, (Bitmap)pictureBox1.Image);
                            }
                        }
                        label2.Text = x_cell_prev+" "+y_cell_prev+" "+ CanPaint(x_cell_prev, y_cell_prev, active_ship_size, povorot).ToString();
                        x_cell_prev = x_cell;
                        y_cell_prev = y_cell;
                    }                    
                }                
            }
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)//очищение поля после убирания мышки
        {
            if (active_ship_id==-1)
            {
                //pictureBox1.Image = this.ColorFillCell(x_cell_prev, y_cell_prev, Color.White, (Bitmap)pictureBox1.Image);
            }
            else if (povorot==false)
            {
                for (int i = 0; i < active_ship_size; i++)
                {
                    pictureBox1.Image = ColorFillCell(x_cell_prev, y_cell_prev + i, Color.White, (Bitmap)pictureBox1.Image);
                }
            }
            else if (povorot)
            {
                for (int i = 0; i < active_ship_size; i++)
                {
                    pictureBox1.Image = ColorFillCell(x_cell_prev + i, y_cell_prev, Color.White, (Bitmap)pictureBox1.Image);
                }
            }
        }

        private void ship4_Click(object sender, EventArgs e)//клик по кораблю, выделение и прочее
        {
            int id = -1;
            int size = 0;
            PictureBox x = (PictureBox)sender;
            ids = x.Tag.ToString().Split();
            if (!int.TryParse(ids[0],out id))
            {
                return;
            }
            if (!int.TryParse(ids[1], out size))
            {
                return;
            }
            if (active_ship_id == -1)
            {
                this.Select((PictureBox) sender);
                active_ship = (PictureBox)sender;
                active_ship_id = id;
            }
            else if (active_ship_id == id)
            {
                this.Deselect((PictureBox)sender);
                active_ship = null;
                active_ship_id = -1;
            }
            else if ((active_ship_id >=0)&&(active_ship_id<10))
            {
                this.Deselect(ships_im[active_ship_id]);
                this.Select((PictureBox)sender);
                active_ship = (PictureBox)sender;
                active_ship_id = id;
            }
            active_ship_size = size;
        }

        private void button3_Click(object sender, EventArgs e)//очистка поля
        {
            Reset();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.ShowDialog();
            string name = f.FileName;
            FileStream fs;
            SavingClass res;
            try
            {
                fs = new FileStream(name,FileMode.Open);
                BinaryFormatter bd = new BinaryFormatter();
                res = (SavingClass)bd.Deserialize(fs);
                parent.res = new result(parent,this,"Конец игры");
                
                parent.b_form = new battlefield(res,this,parent.res);
                Reset();
                parent.b_form.Show();
                this.Hide();
            }
            catch (Exception x)
            {
                //MessageBox.Show(x.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (mult_flag > 0)
            {
                if(!checkBox1.Checked)
                {
                    return;
                }
                int[,] enemy_field_m = new int[10,10];
                for (int i = 0; i < 10;i++)
                {
                    for(int j =0;j<10;j++)
                    {
                        enemy_field_m[i,j]=-1;
                    }
                }
                int diff_m = 4;
                int[,] my_field_m = (int[,])field.Clone();
                Ship[] arg_m = new Ship[10];
                for (int i = 0; i < 10; i++)
                {
                    ships_place[i].image = (Bitmap)ships_im[i].Image;
                    arg_m[i] = ships_place[i].clone();
                }
                if (mult_flag == 2)
                {
                    if (!client.Connected)
                    {
                        return;
                    }
                    parent.b_form = new battlefield(my_field_m, enemy_field_m, 4, this.parent, arg_m, ships_place_enemy, res,client);
                    Reset();
                    parent.b_form.Show();
                    this.Hide();
                    return;
                }
                else if (mult_flag == 1)
                {
                    if (!s.Connected)
                    {
                        return;
                    }
                    parent.b_form = new battlefield(my_field_m, enemy_field_m, 4, this.parent, arg_m, ships_place_enemy, res, s,listener);
                    Reset();
                    parent.b_form.Show();
                    this.Hide();
                    return;
                }
                button2.Enabled = false;
                return;
            }
            int diff = 2;
            if (easy.Checked)
            {
                diff = 1;
            }
            else if (medium.Checked)
            {
                diff = 2;
            }
            else
            {
                diff = 3;
            }
            int[,] enemy_field = GetCompField();
            Ship[] arg = new Ship[10];
            for (int i = 0; i < 10; i++)
            {
                ships_place[i].image = (Bitmap)ships_im[i].Image;
                arg[i] = ships_place[i].clone();
            }
            int[,] test_field = (int[,]) field.Clone();
            parent.res = new result(parent, this, "Конец игры");
            parent.b_form = new battlefield(test_field, enemy_field, diff, this.parent, arg, ships_place_enemy, parent.res);
            Reset();
            parent.b_form.Show();
            this.Hide();

        }

        private void autorasst_Click(object sender, EventArgs e)//авторасстановка
        {
            Reset();
            Random r = new Random();
            int id = 0;
            int buf;
            int x;
            int y;
            povorot = false;
            buf = r.Next(0,2);
            if (buf == 0)
                povorot = false;
            else
                povorot = true;
            if (!povorot)
            {
                x = r.Next(1, 11);
                y = r.Next(1, 8);
            }
            else
            {
                x = r.Next(1, 8);
                y = r.Next(1, 11);
            }
            PlaceShip(x, y, id, (Bitmap)pictureBox1.Image);
            ships_im[id].Enabled = false;
            ships_im[id].Visible = false;
            ships_place[id] = new Ship(x, y, 4, povorot);
            id++;//установка 4 палубного

            List<int>[] coord;
            int size = 3;
            for (int ships = 2; ships<=4;ships++)
            {
                for(int ship_num = 0; ship_num<ships;ship_num++)
                {
                    buf = r.Next(0, 2);
                    if (buf == 0)
                        povorot = false;
                    else
                        povorot = true;//получение случайного флага
                    coord = GetDrawableCells(size, povorot, field);
                    if (coord[0].Count == 0)
                    {
                        povorot = !povorot;
                        coord = GetDrawableCells(3, povorot, field);//получение списка доступных координат
                    }
                    buf = r.Next(0, coord[0].Count);
                    x = coord[0][buf];//случайные координаты из списка
                    y = coord[1][buf];//случайные координаты из списка
                    PlaceShip(x + 1, y + 1, id, (Bitmap)pictureBox1.Image);
                    ships_im[id].Enabled = false;
                    ships_im[id].Visible = false;
                    ships_place[id] = new Ship(x + 1, y + 1, size, povorot);
                    id++;
                }
                size--;
            }
            button2.Enabled = true;
            read.Enabled = true;
        }

        private int[,] GetCompField()
        {
            int[,] res = new int[10, 10];
            for(int i = 0; i< 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    res[i, j] = -1;
                }
            }
            bool buf_povorot = povorot;

            Random r = new Random();
            int id = 0;
            int buf;
            int x;
            int y;
            povorot = false;
            buf = r.Next(0, 2);
            if (buf == 0)
                povorot = false;
            else
                povorot = true;
            if (!povorot)
            {
                x = r.Next(1, 11);
                y = r.Next(1, 8);
            }
            else
            {
                x = r.Next(1, 8);
                y = r.Next(1, 11);
            }
            res = PlaceShip(x,y,id,res,4,povorot);
            ships_place_enemy[id] = new Ship(x, y, 4, povorot);
            ships_place_enemy[id].image = (Bitmap)ships_im[id].Image.Clone();
            id++;//установка 4 палубного

            List<int>[] coord;
            int size = 3;
            //buf = r.Next(0, 2);
            //if (buf == 0)
            //    povorot = false;
            //else
            //    povorot = true;//получение случайного флага
            //coord = GetDrawableCells(size, povorot, res);
            //if (coord[0].Count == 0)
            //{
            //    povorot = !povorot;
            //    coord = GetDrawableCells(3, povorot, res);//получение списка доступных координат
            //}
            //buf = r.Next(0, coord[0].Count);
            //x = coord[0][buf];//случайные координаты из списка
            //y = coord[1][buf];//случайные координаты из списка
            //res = PlaceShip(x + 1, y + 1, id, res, size, povorot);
            //id++;
            for (int ships = 2; ships <= 4; ships++)
            {
                for (int ship_num = 0; ship_num < ships; ship_num++)
                {
                    buf = r.Next(0, 2);
                    if (buf == 0)
                        povorot = false;
                    else
                        povorot = true;//получение случайного флага
                    coord = GetDrawableCells(size, povorot, res);
                    if (coord[0].Count == 0)
                    {
                        povorot = !povorot;
                        coord = GetDrawableCells(3, povorot, res);//получение списка доступных координат
                    }
                    buf = r.Next(0, coord[0].Count);
                    x = coord[0][buf];//случайные координаты из списка
                    y = coord[1][buf];//случайные координаты из списка
                    res = PlaceShip(x + 1, y + 1, id, res, size, povorot);
                    ships_place_enemy[id] = new Ship(x+1, y+1, size, povorot);
                    ships_place_enemy[id].image = (Bitmap)ships_im[id].Image.Clone();
                    id++;
                }
                size--;
            }
            povorot = buf_povorot;
            int count = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if(res[i, j]>=0)
                    {
                        count++;
                    }
                }
            }
            return res;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("help.html");
        }

        private void send(byte[] byte_buff)
        {
            if(mult_flag==2)
            {
                n.Write(byte_buff, 0, byte_buff.Length);
            }
            else if (mult_flag==1)
            {
                s.Send(byte_buff);
            }
        }

        private void read_Click(object sender, EventArgs e)
        {
            byte[] byte_buff = new byte[1];
            byte_buff[0] = 1;
            send(byte_buff);
        }
    }
}
