﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Multiplayer : Form
    {
        public Multiplayer()
        {
            InitializeComponent();
        }
        public Form1 parent;
        public Single single_form;
        public Multiplayer (Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        public Multiplayer(Form1 parent, Single single_form)
        {
            InitializeComponent();
            this.parent = parent;
            this.single_form = single_form;
        }
        private void Back_Click(object sender, EventArgs e)
        {
            parent.Show();
            this.Hide();
        }
        private void Multiplayer_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            parent.Show();
        }

        private void Create_Click(object sender, EventArgs e)
        {
            this.Hide();
            parent.single_form = new Single(parent,this, 1);
            parent.single_form.Show();
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            this.Hide();
            parent.connect_form = new connect(this,parent);
            parent.connect_form.Show();
        }

        private void Multiplayer_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("help.html");
        }
    }
}
