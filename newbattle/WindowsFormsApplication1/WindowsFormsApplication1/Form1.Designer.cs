﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Singleplayer = new System.Windows.Forms.Button();
            this.Multiplayer = new System.Windows.Forms.Button();
            this.SystemAbout = new System.Windows.Forms.Button();
            this.DevelopersAbout = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.Volume = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // Singleplayer
            // 
            this.Singleplayer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Singleplayer.Location = new System.Drawing.Point(612, 127);
            this.Singleplayer.Name = "Singleplayer";
            this.Singleplayer.Size = new System.Drawing.Size(160, 40);
            this.Singleplayer.TabIndex = 0;
            this.Singleplayer.Text = "Однопользовательская игра";
            this.Singleplayer.UseVisualStyleBackColor = true;
            this.Singleplayer.Click += new System.EventHandler(this.Singleplayer_Click);
            // 
            // Multiplayer
            // 
            this.Multiplayer.Location = new System.Drawing.Point(612, 173);
            this.Multiplayer.Name = "Multiplayer";
            this.Multiplayer.Size = new System.Drawing.Size(160, 40);
            this.Multiplayer.TabIndex = 1;
            this.Multiplayer.Text = "Многопользовательская игра";
            this.Multiplayer.UseVisualStyleBackColor = true;
            this.Multiplayer.Click += new System.EventHandler(this.Multiplayer_Click);
            // 
            // SystemAbout
            // 
            this.SystemAbout.Location = new System.Drawing.Point(612, 219);
            this.SystemAbout.Name = "SystemAbout";
            this.SystemAbout.Size = new System.Drawing.Size(160, 40);
            this.SystemAbout.TabIndex = 2;
            this.SystemAbout.Text = "Сведения о системе";
            this.SystemAbout.UseVisualStyleBackColor = true;
            this.SystemAbout.Click += new System.EventHandler(this.SystemAbout_Click);
            // 
            // DevelopersAbout
            // 
            this.DevelopersAbout.Location = new System.Drawing.Point(612, 265);
            this.DevelopersAbout.Name = "DevelopersAbout";
            this.DevelopersAbout.Size = new System.Drawing.Size(160, 40);
            this.DevelopersAbout.TabIndex = 3;
            this.DevelopersAbout.Text = "Сведения о разработчиках";
            this.DevelopersAbout.UseVisualStyleBackColor = true;
            this.DevelopersAbout.Click += new System.EventHandler(this.DevelopersAbout_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(612, 311);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(160, 40);
            this.exit.TabIndex = 4;
            this.exit.Text = "Выход";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // Volume
            // 
            this.Volume.BackColor = System.Drawing.Color.SlateBlue;
            this.Volume.Location = new System.Drawing.Point(670, 535);
            this.Volume.Name = "Volume";
            this.Volume.Size = new System.Drawing.Size(111, 45);
            this.Volume.TabIndex = 5;
            this.Volume.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(794, 576);
            this.Controls.Add(this.Volume);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.DevelopersAbout);
            this.Controls.Add(this.SystemAbout);
            this.Controls.Add(this.Multiplayer);
            this.Controls.Add(this.Singleplayer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "Form1";
            this.Text = "Морской бой";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Singleplayer;
        private System.Windows.Forms.Button Multiplayer;
        private System.Windows.Forms.Button SystemAbout;
        private System.Windows.Forms.Button DevelopersAbout;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.TrackBar Volume;
    }
}

