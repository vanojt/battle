﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class result : Form
    {
        public string text;
        Form parent_menu;
        Form parent_single;
        public battlefield field;
        public result()
        {
            InitializeComponent();
        }

        public result(Form parent_m, Form parent_s, string mess)
        {
            parent_single = parent_s;
            parent_menu = parent_m;
            text = mess;
            InitializeComponent();
        }

        public void SetString(String s)
        {
            text = s;
            label1.Text = s;
        }

        private void result_Load(object sender, EventArgs e)
        {
            label1.Text = text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                field.Hide();
            }
            catch { }
            this.Hide();
            parent_single.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                field.Hide();
            }
            catch { }
            this.Hide();
            parent_menu.Show();
        }
    }
}
