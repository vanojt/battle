﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApplication1
{
    [Serializable]
    public class Ship
    {
        public int x_cell = 1;
        public int y_cell = 1;
        public bool povorot = false;
        public int ship_size = 1;
        public Bitmap image;
        public int damaged = 0;

        public Ship(int x_cell_new, int y_cell_new, int ship_size_new, bool povorot_new)
        {
            x_cell = x_cell_new;
            y_cell = y_cell_new;
            ship_size = ship_size_new;
            povorot = povorot_new;
        }

        public Ship(int x_cell_new, int y_cell_new, int ship_size_new, bool povorot_new, Bitmap img)
        {
            x_cell = x_cell_new;
            y_cell = y_cell_new;
            ship_size = ship_size_new;
            povorot = povorot_new;
            image = img;
        }

        public Ship clone()
        {
            return new Ship(x_cell,y_cell,ship_size,povorot,(Bitmap)image.Clone());
        }
    }
}
