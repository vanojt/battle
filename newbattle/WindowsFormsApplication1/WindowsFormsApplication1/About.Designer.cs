﻿namespace WindowsFormsApplication1
{
    partial class DevelopersAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevelopersAbout));
            this.Single_FormClosed = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Single_FormClosed
            // 
            this.Single_FormClosed.Location = new System.Drawing.Point(99, 165);
            this.Single_FormClosed.Name = "Single_FormClosed";
            this.Single_FormClosed.Size = new System.Drawing.Size(191, 27);
            this.Single_FormClosed.TabIndex = 0;
            this.Single_FormClosed.Text = "Назад";
            this.Single_FormClosed.UseVisualStyleBackColor = true;
            this.Single_FormClosed.Click += new System.EventHandler(this.Single_FormClosed_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(357, 147);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DevelopersAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 198);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Single_FormClosed);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DevelopersAbout";
            this.Text = "О разработчиках";
            this.Load += new System.EventHandler(this.DevelopersAbout_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Single_FormClosed;
        private System.Windows.Forms.TextBox textBox1;
    }
}