﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public battlefield b_form;
        public Single single_form;
        public result res;
        private DevelopersAbout developersAbout_form;
        private Multiplayer multiplayer_form;
        public connect connect_form;
        public help rules;

        public Form1()
        {
            InitializeComponent();
        }

        public void CloseAll()
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            single_form = new Single(this);
            developersAbout_form = new DevelopersAbout(this);
            multiplayer_form = new Multiplayer(this,single_form);
            res = new result(this, single_form, "конец игры");
            single_form.res = res;
            rules = new help();
        }
     
        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Singleplayer_Click(object sender, EventArgs e)
        {
            this.Hide();
            single_form = new Single(this);
            single_form.Show();
        }

        private void DevelopersAbout_Click(object sender, EventArgs e)
        {
            developersAbout_form = new DevelopersAbout();
            developersAbout_form.Show();
        }

        private void Multiplayer_Click(object sender, EventArgs e)
        {
           this.Hide();
           multiplayer_form.Show();
        }

        private void SystemAbout_Click(object sender, EventArgs e)
        {
            Process.Start("help.html");
        }
    }
}
