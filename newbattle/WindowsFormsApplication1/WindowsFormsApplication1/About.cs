﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class DevelopersAbout : Form
    {
        public DevelopersAbout()
        {
            InitializeComponent();
        }
    private Form1 parent;
        public DevelopersAbout (Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
        }
       
        private void Back_Click(object sender, EventArgs e)
        {
            parent.Show();
            this.Hide();
        }

        private void Single_FormClosed_Click(object sender, EventArgs e)
        {
            parent.Close();
        }

        private void Single_FormClosed_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DevelopersAbout_Load(object sender, EventArgs e)
        {

        }

        
    }
}
