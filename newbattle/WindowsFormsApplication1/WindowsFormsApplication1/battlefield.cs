﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class battlefield : Form
    {
        Thread thr, thr1;
        byte[] byte_buff;
        bool music = false;
        int curr_vol;
        public int difficulty = 1;
        public Form parent;
        bool dobiv = false;
        bool vertical = false;
        public result res;
        public int[,] my_field;//10+ - подбит 20 - мимо 21 - заблокировано
        public int[,] enemy_field;
        public Ship[] ship_place;
        public Ship[] ship_place_enemy;
        private List<int>[] coords = new List<int>[2];
        int ind = 0;
        int x_cell = 1;
        int y_cell = 1;
        int x_cell_prev = -1;
        int y_cell_prev = -1;
        int x_cell_first = -1;
        int y_cell_first = -1;
        bool turn = true;
        int cells_count_enemy = 20;
        int cells_count_my = 20;
        int buf;
        List<int> areal_x = new List<int>();
        List<int> areal_y = new List<int>();
        TcpClient client;
        NetworkStream n;
        Socket s;
        TcpListener listener;
        Random r = new Random();
        System.Media.SoundPlayer plr = new System.Media.SoundPlayer();
        

        Bitmap fire = (Bitmap)Bitmap.FromFile("../../fire.png");


        public battlefield(SavingClass sav, Form parent,result res)
        {
            difficulty = sav.difficulty;
            this.parent = parent;
            dobiv = sav.dobiv;
            my_field = sav.my_field;
            enemy_field = sav.enemy_field;
            ship_place = sav.ship_place;
            ship_place_enemy = sav.ship_place_enemy;
            ind = sav.ind;
            x_cell = sav.x_cell;
            y_cell = sav.y_cell;
            x_cell_first = sav.x_cell_first;
            y_cell_first = sav.y_cell_first;
            x_cell_prev = sav.x_cell_prev;
            y_cell_prev = sav.y_cell_prev;
            turn = sav.turn;
            cells_count_enemy = sav.cells_count_enemy;
            cells_count_my = sav.cells_count_my;
            if (sav.areal_x != null)
            {
                areal_x = sav.areal_x;
            }
            if(sav.areal_y!=null)
            {
                areal_y = sav.areal_y;
            }
            InitializeComponent();
            MyField.Image = sav.MyField;
            EnemyField.Image = sav.EnemyField;
            this.res = res;
            
        }
        public battlefield()
        {
            InitializeComponent();
        }

        public battlefield(int[,] my_field_n, int[,]enemy_field_n,int diff, Form1 parent_n,Ship[] ships_n, Ship[] ships_enemy_n,result res_n)
        {
            my_field = my_field_n;
            enemy_field = enemy_field_n;
            parent = parent_n;
            ship_place = ships_n;
            difficulty = diff;
            ship_place_enemy = ships_enemy_n;
            res = res_n;
            InitializeComponent();
        }

        public battlefield(int[,] my_field_n, int[,] enemy_field_n, int diff, Form1 parent_n, Ship[] ships_n, Ship[] ships_enemy_n, result res_n, TcpClient client )
        {
            my_field = my_field_n;
            enemy_field = enemy_field_n;
            parent = parent_n;
            ship_place = ships_n;
            difficulty = diff;
            ship_place_enemy = ships_enemy_n;
            res = res_n;
            this.client = client;
            turn = false;
            n = client.GetStream();
            thr = new Thread(ConnectionClient);
            thr.Start();
            InitializeComponent();
            checkBox1.Checked = false;
        }

        public battlefield(int[,] my_field_n, int[,] enemy_field_n, int diff, Form1 parent_n, Ship[] ships_n, Ship[] ships_enemy_n, result res_n, Socket s, TcpListener listener)
        {
            my_field = my_field_n;
            enemy_field = enemy_field_n;
            parent = parent_n;
            ship_place = ships_n;
            difficulty = diff;
            ship_place_enemy = ships_enemy_n;
            res = res_n;
            thr = new Thread(ConnectionServer);
            thr.Start();
            InitializeComponent();
            checkBox1.Checked = true;
        }

        public void ConnectionClient()
        {
            byte[] byte_buff = new byte[2];
            try
            {
                while (true)
                {
                    n.Read(byte_buff, 0, 2);
                }
            }
            catch (Exception e)
            { //MessageBox.Show(e.Message);}
            }
        }
        public void ConnectionServer()
        {
            //try
            //{
            //    byte[] buffer_sock = new byte[2];
            //    while (true)
            //    {
            //        s.Receive(buffer_sock);
            //        object[] args = new object[1];
            //        args[0] = buffer_sock;
            //        if(MyField.InvokeRequired)
            //        {
            //            MyField.BeginInvoke(shot1(shot), args);
            //        }
            //    }
            //}
            //catch(Exception e)
            //{

            //}
        }

        public delegate void shot1();

        public void shot(byte[] coords)
        {

        }

        public static Bitmap PutFire(int x, int y, Bitmap fire, Bitmap map)//(индекс с 1)залить ячейку цветом
        {
            x--;
            y--;
            int i_begin = x * 35 + 1;
            int i_end = (x + 1) * 35;
            int j_begin = y * 35 + 1;
            int j_end = (y + 1) * 35;
            for (int i = i_begin; i < i_end; i++)
            {
                for (int j = j_begin; j < j_end; j++)
                {
                    map.SetPixel(i, j, fire.GetPixel(i - i_begin,j-j_begin));
                }
            }
            return map;
        }

        private void battlefield_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < 10; i++)
            {
                MyField.Image = PlaceShip(ship_place[i].x_cell, ship_place[i].y_cell, i, (Bitmap)MyField.Image);
            }
            plr.SoundLocation = "../../gun9.wav";
            plr.Load();
        }

        private List<int>[] GetCoordList(int[,] field)
        {
            List<int>[] coords = new List<int>[2];
            List<int> coords_x = new List<int>();
            List<int> coords_y = new List<int>();
            coords[0] = coords_x;
            coords[1] = coords_y;

            for (int i = 0; i < 10; i++ )
            {
                for(int j = 0; j<10;j++)
                {
                    if (field[i,j]<10)
                    {
                        coords_x.Add(i);
                        coords_y.Add(j);
                    }
                }
            }
            return coords;
        }

        private Bitmap KillShip(int[,] field, int ship_id, Ship boat, Bitmap map)
        {
            int x_cell = boat.x_cell-1;
            int y_cell = boat.y_cell-1;
            bool povorot = boat.povorot;
            int ship_size = boat.ship_size;

            if(!povorot)
            {
                if (x_cell+1<10)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + 1, y_cell + i] = 21;
                        map = Single.ColorFillCell(x_cell + 2, y_cell + i+1, Color.Gray, map);
                    }
                }
                if(x_cell-1>=0)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell - 1, y_cell + i] = 21;
                        map = Single.ColorFillCell(x_cell, y_cell + i +1, Color.Gray, map);
                    }
                }
                if (y_cell+ship_size<10)
                {
                    field[x_cell, y_cell + ship_size] = 21;
                    map = Single.ColorFillCell(x_cell+1, y_cell + ship_size+1, Color.Gray, map);
                    if(x_cell+1<10)
                    {
                        field[x_cell + 1, y_cell + ship_size] = 21;
                        map = Single.ColorFillCell(x_cell + 2, y_cell + ship_size + 1, Color.Gray, map);
                    }
                    if(x_cell-1>=0)
                    {
                        field[x_cell -1, y_cell + ship_size] = 21;
                        map = Single.ColorFillCell(x_cell, y_cell + ship_size + 1, Color.Gray, map);
                    }
                }
                if (y_cell - 1 >= 0)
                {
                    field[x_cell, y_cell - 1] = 21;
                    map = Single.ColorFillCell(x_cell + 1, y_cell, Color.Gray, map);
                    if (x_cell + 1 < 10)
                    {
                        field[x_cell + 1, y_cell - 1] = 21;
                        map = Single.ColorFillCell(x_cell + 2, y_cell, Color.Gray, map);
                    }
                    if (x_cell - 1 >= 0)
                    {
                        field[x_cell - 1, y_cell - 1] = 21;
                        map = Single.ColorFillCell(x_cell, y_cell, Color.Gray, map);
                    }
                }
            }
            else
            {
                if (y_cell + 1 < 10)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + i, y_cell + 1] = 21;
                        map = Single.ColorFillCell(x_cell + 1 + i, y_cell+2, Color.Gray, map);
                    }
                }
                if (y_cell - 1 >= 0)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + i, y_cell - 1] = 21;
                        map = Single.ColorFillCell(x_cell + 1 + i, y_cell, Color.Gray, map);
                    }
                }
                if (x_cell + ship_size < 10)
                {
                    field[x_cell + ship_size, y_cell] = 21;
                    map = Single.ColorFillCell(x_cell + ship_size +1, y_cell+1, Color.Gray, map);
                    if (y_cell + 1 < 10)
                    {
                        field[x_cell + ship_size, y_cell + 1] = 21;
                        map = Single.ColorFillCell(x_cell + ship_size + 1, y_cell + 2, Color.Gray, map);
                    }
                    if (y_cell - 1 >= 0)
                    {
                        field[x_cell + ship_size, y_cell - 1] = 21;
                        map = Single.ColorFillCell(x_cell + ship_size + 1, y_cell, Color.Gray, map);
                    }
                }
                if (x_cell - 1 >= 0)
                {
                    field[x_cell - 1, y_cell] = 21;
                    map = Single.ColorFillCell(x_cell, y_cell + 1, Color.Gray, map);
                    if (y_cell + 1 < 10)
                    {
                        field[x_cell - 1, y_cell + 1] = 21;
                        map = Single.ColorFillCell(x_cell, y_cell + 2, Color.Gray, map);
                    }
                    if (y_cell - 1 >= 0)
                    {
                        field[x_cell - 1, y_cell - 1] = 21;
                        map = Single.ColorFillCell(x_cell, y_cell, Color.Gray, map);
                    }
                }
            }

            return map;
        }

        private int[,] KillShip(int[,] field, int ship_id, Ship boat)
        {
            int x_cell = boat.x_cell - 1;
            int y_cell = boat.y_cell - 1;
            bool povorot = boat.povorot;
            int ship_size = boat.ship_size;

            if (!povorot)
            {
                if (x_cell + 1 < 10)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + 1, y_cell + i] = 21;
                    }
                }
                if (x_cell - 1 >= 0)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell - 1, y_cell + i] = 21;
                    }
                }
                if (y_cell + ship_size < 10)
                {
                    field[x_cell, y_cell + ship_size] = 21;
                    if (x_cell + 1 < 10)
                    {
                        field[x_cell + 1, y_cell + ship_size] = 21;
                    }
                    if (x_cell - 1 >= 0)
                    {
                        field[x_cell - 1, y_cell + ship_size] = 21;
                    }
                }
                if (y_cell - 1 >= 0)
                {
                    field[x_cell, y_cell - 1] = 21;
                    if (x_cell + 1 < 10)
                    {
                        field[x_cell + 1, y_cell - 1] = 21;
                    }
                    if (x_cell - 1 >= 0)
                    {
                        field[x_cell - 1, y_cell - 1] = 21;
                    }
                }
            }
            else
            {
                if (y_cell + 1 < 10)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + i, y_cell + 1] = 21;
                    }
                }
                if (y_cell - 1 >= 0)
                {
                    for (int i = 0; i < ship_size; i++)
                    {
                        field[x_cell + i, y_cell - 1] = 21;
                    }
                }
                if (x_cell + ship_size < 10)
                {
                    field[x_cell + ship_size, y_cell] = 21;
                    if (y_cell + 1 < 10)
                    {
                        field[x_cell + ship_size, y_cell + 1] = 21;
                    }
                    if (y_cell - 1 >= 0)
                    {
                        field[x_cell + ship_size, y_cell - 1] = 21;
                    }
                }
                if (x_cell - 1 >= 0)
                {
                    field[x_cell - 1, y_cell] = 21;
                    if (y_cell + 1 < 10)
                    {
                        field[x_cell - 1, y_cell + 1] = 21;
                    }
                    if (y_cell - 1 >= 0)
                    {
                        field[x_cell - 1, y_cell - 1] = 21;
                    }
                }
            }

            return field;
        }
        public Bitmap PlaceShip(int x, int y, int ship_id, Bitmap map)//индекс с 1
        {
            Bitmap image = (Bitmap)ship_place[ship_id].image;
            int size = ship_place[ship_id].ship_size;
            if (!ship_place[ship_id].povorot)
            {
                x--;//чтоб соответствовать индексу
                y--;//чтоб соответствовать индексу
                int i_begin = x * 35 + 1;
                int i_end = i_begin + image.Width;

                int j_begin = y * 35 + 1;
                int j_end = j_begin + image.Height;
                for (int i = i_begin; i < i_end; i++)
                {
                    for (int j = j_begin; j < j_end; j++)
                    {
                        if (j % 35 == 0)//проверка на сетку
                        {
                            map.SetPixel(i, j, Color.Black);
                        }
                        else
                        {
                            map.SetPixel(i, j, image.GetPixel(i - i_begin, j - j_begin));
                        }
                    }
                }
            }
            else
            {
                x--;//чтоб соответствовать индексу
                y--;//чтоб соответствовать индексу
                int i_begin = x * 35 + 1;
                int i_end = i_begin + image.Height;

                int j_begin = y * 35 + 1;
                int j_end = j_begin + image.Width;
                for (int i = i_begin; i < i_end; i++)
                {
                    for (int j = j_begin; j < j_end; j++)
                    {
                        if (i % 35 == 0)//проверка на сетку
                        {
                            map.SetPixel(i, j, Color.Black);
                        }
                        else
                        {
                            map.SetPixel(i, j, image.GetPixel(j - j_begin, i - i_begin));
                        }
                    }
                }
            }
            return map;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            parent.Show();
            this.Hide();
        }

        private void battlefield_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.Close();
        }

        private void EnemyField_Click(object sender, EventArgs e)
        {
            if(difficulty == 4)
            {
                if(checkBox1.Checked==false)//если не наш ход
                {
                    return;
                }
                if(n!=null)//через n
                {
                    byte_buff = new byte[2];
                    byte_buff[0]=(byte)x_cell;
                    byte_buff[1]=(byte)y_cell;
                    n.Write(byte_buff,0,2);//отправка
                    byte_buff[0] = 0;
                    byte_buff[1] = 0;
                    n.Read(byte_buff,0,1);
                    if (byte_buff[0]==1)
                    {
                        EnemyField.Image = PutFire(x_cell, y_cell, fire, (Bitmap)EnemyField.Image);
                        cells_count_enemy--;
                    }
                    else if(byte_buff[0]==-1)
                    {
                        EnemyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Gray, (Bitmap)EnemyField.Image);
                        turn = false;
                    }
                }
                else if (s!=null)//через s
                {

                }
            }
            if (enemy_field[x_cell - 1, y_cell - 1] >= 10)//если ткнул в неактивную клетку
            {
                return;
            }
            if(music)
            {
                plr.Play();
            }
            if ((enemy_field[x_cell - 1, y_cell - 1]>=0)&&(enemy_field[x_cell - 1, y_cell - 1]<10))
            {
                cells_count_enemy--;
                //EnemyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Red, (Bitmap)EnemyField.Image);
                EnemyField.Image = PutFire(x_cell, y_cell, fire, (Bitmap)EnemyField.Image);
                ship_place_enemy[enemy_field[x_cell - 1, y_cell - 1]].damaged += 1;
                enemy_field[x_cell - 1, y_cell - 1] = enemy_field[x_cell - 1, y_cell - 1] + 10;
                int damaged = ship_place_enemy[enemy_field[x_cell - 1, y_cell - 1]-10].damaged;
                int size = ship_place_enemy[enemy_field[x_cell - 1, y_cell - 1]-10].ship_size;
                if (damaged == size)
                {
                    EnemyField.Image = (Bitmap)KillShip(
                        enemy_field, //поле
                        enemy_field[x_cell - 1, y_cell - 1] - 10,//id
                        ship_place_enemy[enemy_field[x_cell - 1, y_cell - 1] - 10],//объект корабля
                        (Bitmap)EnemyField.Image);//изображение поля
                }
                if(cells_count_enemy==0)
                {
                    button1.Enabled = false;
                    button4.Enabled = false;
                    button5.Enabled = false;
                    MyField.Enabled = false;
                    EnemyField.Enabled = false;
                    res.field = this;
                    res.SetString("Вы выиграли");
                    res.text = "Вы выиграли";
                    //this.Hide();
                    res.Show();
                }
            }
            else
            {
                EnemyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Gray, (Bitmap)EnemyField.Image);
                enemy_field[x_cell - 1, y_cell - 1] = 20;
                turn = false;
                while(!turn)
                {
                    if (difficulty == 1)//лёгкая сложность
                    {
                        coords = GetCoordList(my_field);
                        buf = r.Next(0, coords[0].Count);
                        x_cell = coords[0][buf]+1;
                        y_cell = coords[1][buf]+1;
                        if ((my_field[x_cell - 1, y_cell - 1] >= 0) && (my_field[x_cell - 1, y_cell - 1] < 10))//если попал
                        {
                            cells_count_my--;
                            MyField.Image = PutFire(x_cell, y_cell, fire, (Bitmap)MyField.Image);
                            ship_place[my_field[x_cell - 1, y_cell - 1]].damaged += 1;//увеличение числа поврежденных клеток
                            my_field[x_cell - 1, y_cell - 1] = my_field[x_cell - 1, y_cell - 1] + 10;//переносим клетку к подбитым
                            int damaged = ship_place[my_field[x_cell - 1, y_cell - 1] - 10].damaged;
                            int size = ship_place[my_field[x_cell - 1, y_cell - 1] - 10].ship_size;
                            if (damaged == size)
                            {
                                MyField.Image = (Bitmap)KillShip(
                                    my_field, //поле
                                    my_field[x_cell - 1, y_cell - 1] - 10,//id
                                    ship_place[my_field[x_cell - 1, y_cell - 1] - 10],//объект корабля
                                    (Bitmap)MyField.Image);//изображение поля
                            }
                            if (cells_count_my == 0)
                            {
                                button1.Enabled = false;
                                button4.Enabled = false;
                                button5.Enabled = false;
                                MyField.Enabled = false;
                                EnemyField.Enabled = false;
                                res.field = this;
                                res.SetString("Вы проиграли");
                                res.text = "Вы проиграли";
                                //this.Hide();
                                res.Show();
                            }
                        }
                        else
                        {
                            MyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Gray, (Bitmap)MyField.Image);
                            my_field[x_cell - 1, y_cell - 1] = 20;
                            turn = true;
                        }
                    }
                    else if ((difficulty == 2)||(difficulty == 3))//средняя сложность и высокая
                    {
                        if(dobiv)
                        {
                            if ((my_field[areal_x[ind]-1,areal_y[ind]-1]>=0)&&(my_field[areal_x[ind]-1,areal_y[ind]-1]<10))
                            {
                                cells_count_my--;
                                ship_place[my_field[areal_x[ind] - 1, areal_y[ind] - 1]].damaged += 1;//увеличение числа поврежденных клеток
                                my_field[areal_x[ind] - 1, areal_y[ind] - 1] = my_field[areal_x[ind] - 1, areal_y[ind] - 1] + 10;//переносим клетку к подбитым
                                int damaged = ship_place[my_field[areal_x[ind] - 1, areal_y[ind] - 1] - 10].damaged;
                                int size = ship_place[my_field[areal_x[ind] - 1, areal_y[ind] - 1] - 10].ship_size;
                                if(damaged==size)//устраненение моего корабля
                                {
                                    MyField.Image = PutFire(areal_x[ind], areal_y[ind], fire, (Bitmap)MyField.Image);
                                    MyField.Image = (Bitmap)KillShip(
                                        my_field, //поле
                                        my_field[areal_x[ind] - 1, areal_y[ind] - 1] - 10,//id
                                        ship_place[my_field[areal_x[ind] - 1, areal_y[ind] - 1] - 10],//объект корабля
                                        (Bitmap)MyField.Image);//изображение поля
                                    dobiv = false;
                                    ind = 0;
                                }
                                else
                                {
                                    x_cell = areal_x[ind];
                                    y_cell = areal_y[ind];
                                    x_cell_prev = x_cell;
                                    y_cell_prev = y_cell;

                                    if (x_cell - x_cell_first == 0)
                                    {
                                        vertical = true;
                                    }
                                    else
                                    {
                                        vertical = false;
                                    }
                                    areal_x.Clear();
                                    areal_y.Clear();
                                    if (vertical)
                                    {
                                        if ((y_cell - 1 >= 1) && (my_field[x_cell - 1, y_cell - 2] < 20))
                                        {
                                            areal_x.Add(x_cell);
                                            areal_y.Add(y_cell - 1);
                                        }
                                        if ((y_cell + 1 <= 10) && (my_field[x_cell - 1, y_cell] < 20))
                                        {
                                            areal_x.Add(x_cell);
                                            areal_y.Add(y_cell + 1);
                                        }
                                    }
                                    else
                                    {
                                        areal_x.Clear();
                                        areal_y.Clear();
                                        if ((x_cell - 1 >= 1) && (my_field[x_cell - 2, y_cell - 1] < 20))
                                        {
                                            areal_x.Add(x_cell - 1);
                                            areal_y.Add(y_cell);
                                        }
                                        if ((x_cell + 1 <= 10) && (my_field[x_cell, y_cell - 1] < 20))
                                        {
                                            areal_x.Add(x_cell + 1);
                                            areal_y.Add(y_cell);
                                        }
                                    }
                                    MyField.Image = PutFire(x_cell_prev, y_cell_prev, fire, (Bitmap)MyField.Image);
                                    ind = 0;
                                }
                            }
                            else
                            {
                                x_cell = areal_x[ind];
                                y_cell = areal_y[ind];
                                if (my_field[x_cell-1,y_cell-1]<0)
                                {
                                    MyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Gray, (Bitmap)MyField.Image);
                                    my_field[x_cell - 1, y_cell - 1] = 20;
                                }
                                ind++;
                                if(ind == areal_x.Count)
                                {
                                    if (vertical)
                                    {
                                        if ((y_cell_first - 1 >= 1) && (my_field[x_cell_first - 1, y_cell_first - 2] < 20))
                                        {
                                            areal_x.Add(x_cell_first);
                                            areal_y.Add(y_cell_first - 1);
                                        }
                                        if ((y_cell_first + 1 <= 10) && (my_field[x_cell_first - 1, y_cell] < 20))
                                        {
                                            areal_x.Add(x_cell_first);
                                            areal_y.Add(y_cell_first + 1);
                                        }
                                    }
                                    else
                                    {
                                        if ((x_cell - 1 >= 1) && (my_field[x_cell_first - 2, y_cell_first - 1] < 20))
                                        {
                                            areal_x.Add(x_cell_first - 1);
                                            areal_y.Add(y_cell_first);
                                        }
                                        if ((x_cell_first + 1 <= 10) && (my_field[x_cell_first, y_cell_first - 1] < 20))
                                        {
                                            areal_x.Add(x_cell_first + 1);
                                            areal_y.Add(y_cell_first);
                                        }
                                    }
                                }
                                turn = true;
                            }
                            if (cells_count_my == 0)
                            {
                                button1.Enabled = false;
                                button4.Enabled = false;
                                button5.Enabled = false;
                                MyField.Enabled = false;
                                EnemyField.Enabled = false;
                                res.field = this;
                                res.SetString("Вы проиграли");
                                res.text = "Вы проиграли";
                                //this.Hide();
                                res.Show();
                            }
                        }
                        else
                        {
                            coords = GetCoordList(my_field);
                            buf = r.Next(0, coords[0].Count);
                            x_cell = coords[0][buf] + 1;
                            y_cell = coords[1][buf] + 1;
                            if ((my_field[x_cell - 1, y_cell - 1] >= 0) && (my_field[x_cell - 1, y_cell - 1] < 10))//если попал
                            {
                                cells_count_my--;
                                areal_x.Clear();
                                areal_y.Clear();
                                x_cell_first = x_cell;
                                y_cell_first = y_cell;
                                x_cell_prev = x_cell;
                                y_cell_prev = y_cell;

                                if ((x_cell - 1 >=1)&&(my_field[x_cell-2,y_cell-1]<20))
                                {
                                    areal_x.Add(x_cell - 1);
                                    areal_y.Add(y_cell);
                                }
                                if ((x_cell + 1 <= 10)&&(my_field[x_cell,y_cell-1]<20))
                                {
                                    areal_x.Add(x_cell + 1);
                                    areal_y.Add(y_cell);
                                }
                                if ((y_cell - 1 >= 1) && (my_field[x_cell - 1, y_cell - 2] < 20))
                                {
                                    areal_x.Add(x_cell);
                                    areal_y.Add(y_cell-1);
                                }
                                if ((y_cell + 1 <= 10) && (my_field[x_cell - 1, y_cell] < 20))
                                {
                                    areal_x.Add(x_cell);
                                    areal_y.Add(y_cell + 1);
                                }
                                MyField.Image = PutFire(x_cell, y_cell, fire, (Bitmap)MyField.Image);
                                ship_place[my_field[x_cell - 1, y_cell - 1]].damaged += 1;//увеличение числа поврежденных клеток
                                my_field[x_cell - 1, y_cell - 1] = my_field[x_cell - 1, y_cell - 1] + 10;//переносим клетку к подбитым
                                int damaged = ship_place[my_field[x_cell - 1, y_cell - 1] - 10].damaged;
                                int size = ship_place[my_field[x_cell - 1, y_cell - 1] - 10].ship_size;
                                if (damaged == size)
                                {
                                    MyField.Image = (Bitmap)KillShip(
                                        my_field, //поле
                                        my_field[x_cell - 1, y_cell - 1] - 10,//id
                                        ship_place[my_field[x_cell - 1, y_cell - 1] - 10],//объект корабля
                                        (Bitmap)MyField.Image);//изображение поля
                                }
                                else
                                {
                                    dobiv = true;
                                }
                                if (cells_count_my == 0)
                                {
                                    button1.Enabled = false;
                                    button4.Enabled = false;
                                    button5.Enabled = false;
                                    MyField.Enabled = false;
                                    EnemyField.Enabled = false;
                                    res.field = this;
                                    res.SetString("Вы проиграли");
                                    res.text = "Вы проиграли";
                                    //this.Hide();
                                    res.Show();
                                }
                            }
                            else
                            {
                                MyField.Image = Single.ColorFillCell(x_cell, y_cell, Color.Gray, (Bitmap)MyField.Image);
                                my_field[x_cell - 1, y_cell - 1] = 20;
                                turn = true;
                            }
                        }
                    }
                }
            }
        }

        private void EnemyField_MouseMove(object sender, MouseEventArgs e)
        {
            x_cell = Single.GetCoordX(e);
            y_cell = Single.GetCoordY(e);
            label1.Text = x_cell + " " + y_cell;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = "";
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j< 10; j++)
                {
                    s+=my_field[j,i]+" ";
                }
                s += "\n";
            }
            MessageBox.Show(s);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string s = "";
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    s += enemy_field[j, i] + " ";
                }
                s += "\n";
            }
            MessageBox.Show(s);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SavingClass sav = new SavingClass();
            sav.difficulty = difficulty;
            sav.dobiv = dobiv;
            sav.my_field = my_field;
            sav.enemy_field = enemy_field;
            sav.ship_place = ship_place;
            sav.ship_place_enemy = ship_place_enemy;
            sav.x_cell = x_cell;
            sav.y_cell = y_cell;
            sav.x_cell_prev = x_cell_prev;
            sav.y_cell_prev = y_cell_prev;
            sav.x_cell_first = x_cell_first;
            sav.y_cell_first = y_cell_first;
            sav.turn = turn;
            sav.cells_count_enemy = cells_count_enemy;
            sav.cells_count_my = cells_count_my;
            sav.MyField = (Bitmap)MyField.Image.Clone();
            sav.EnemyField = (Bitmap)EnemyField.Image.Clone();
            sav.areal_x = areal_x;
            sav.areal_y = areal_y;
            sav.ind = ind;
            SaveFileDialog f = new SaveFileDialog();
            f.ShowDialog();
            string name = f.FileName + ".battle";
            FileStream fs = new FileStream(name, FileMode.Create);
            BinaryFormatter form = new BinaryFormatter();
            form.Serialize(fs, sav);
            fs.Close();
            fs = new FileStream(name,FileMode.Open);
            SavingClass sav2 = (SavingClass) form.Deserialize(fs);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (trackBar1.Value==0)
            {
                music = false;
            }
            else
            {
                music = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("help.html");
        }
    }
}
