﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    [Serializable]
    public class SavingClass
    {
        public SavingClass()
        {}
        public int difficulty = 1;
        public bool dobiv = false;
        public int[,] my_field;
        public int[,] enemy_field;
        public Ship[] ship_place;
        public Ship[] ship_place_enemy;
        public int x_cell = 1;
        public int y_cell = 1;
        public int x_cell_prev = -1;
        public int y_cell_prev = -1;
        public int x_cell_first = -1;
        public int y_cell_first = -1;
        public bool turn = true;
        public int cells_count_enemy = 20;
        public int cells_count_my = 20;
        public Bitmap MyField;
        public Bitmap EnemyField;
        public List<int> areal_x;
        public List<int> areal_y;
        public int ind;
    }
}
